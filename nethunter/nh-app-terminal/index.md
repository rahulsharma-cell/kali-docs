---
title: NetHunter Application - Terminal
description:
icon:
date: 2019-11-29
type: post
weight: 230
author: ["re4son",]
tags: ["",]
keywords: ["",]
og_description:
---

This application allows you to open up one of several kinds of terminals - a chrooted Kali terminal, a standard Android terminal, and a root Android terminal.

![](./nethunter-terminal.png)
